<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
/*************************************************/
/*GRUPOS                                         */
/*************************************************/
$routes->get('/listado/hoteles', 'TipohabitacionController::hoteles'); // MOSTRAR GRUPOS

$routes->get('/borrar/grupo/(:alphanum)', 'TipohabitacionController::borrarHotel/$1'); // BORRAR GRUPO

$routes->get('/insertar/hotel', 'TipohabitacionController::formHoteles'); // INSERTAR GRUPOS
$routes->post('/insertar/creaHotel', 'TipohabitacionController::insertaHotel'); // INSERTAR GRUPOS

$routes->get('editar/grupo/(:num)', 'TipohabitacionController::grupos/$1'); // EDITAR GRUPO
$routes->post('editar/editogrupo/(:num)', 'TipohabitacionController::editoGrupo/$1'); // EDITAR GRUPO

/*************************************************/
/*ALUMNOS                                         */
/*************************************************/
$routes->get('/borrar/alumno/(:alphanum)', 'TipohabitacionController::alumno/$1'); // BORRAR ALUMNO

$routes->get('/insertar/alumno', 'TipohabitacionController::formAlumnos'); // INSERTAR ALUMNO
$routes->post('/insertar/creaAlumno', 'TipohabitacionController::insertaAlumno'); // INSERTAR ALUMNO

$routes->get('editar/alumnos/(:num)', 'TipohabitacionController::alumnos/$1'); // EDITAR ALUMNO
$routes->post('editar/editoAlumno/(:num)', 'TipohabitacionController::editoAlumno/$1'); // EDITAR ALUMNO

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
