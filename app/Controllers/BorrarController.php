<?php


namespace App\Controllers;
use App\Models\GrupoModel; 
use App\Models\AlumnoModel; 


class BorrarController extends BaseController {
    
    public function grupo($codigo){
        $grupoModel = new GrupoModel();
        $data['grupo'] = $grupoModel->where('codigo', $codigo)->delete();
        return  redirect()->to('listado/grupos');
    }
    
    public function alumno($NIA){
        $alumnoModel = new AlumnoModel();
        $data['alumno'] = $alumnoModel->where('NIA', $NIA)->delete();
        return redirect()->back();
    }
}