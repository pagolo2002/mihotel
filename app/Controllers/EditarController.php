<?php


namespace App\Controllers;
use App\Models\GrupoModel; 
use App\Models\AlumnoModel; 


class EditarController extends BaseController {
    
    public function grupos($id){
        helper('form');
        $gruposModel = new GrupoModel();
        $data['title'] = 'Modificar Grupo';
        $data['grupo'] = $gruposModel->find($id);
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        return view('grupos/editarGrupos',$data);
    }
    
    public function editoGrupo($id){
        
        helper('form');
        $gruposModel = new GrupoModel();
        $data['title'] = 'Modificar Grupo';
        $data['grupo'] = $gruposModel->find($id);
        $grupo = $this->request->getPost();
        unset($grupo['botoncito']);
        /*
        echo '<pre>';
             print_r($grupo);
        echo '</pre>';
        */
        $grupoModel = new GrupoModel();
        $grupoModel->update($id,$grupo);
        
        if ($grupoModel->update($id,$grupo)=== false){
            $data['errores'] = $grupoModel->errors();
            $data['title'] = 'Modificar Grupo';
            return view('grupos/editarGrupos',$data);
        }
        
        return redirect('listado/grupos');
    }
    
    public function alumnos($id){
        helper('form');
        $alumnoModel = new AlumnoModel();
        $data['title'] = 'Modificar Alumno';
        //$data['alumno'] = $alumnoModel->where('NIA', $nia)->find();
        $data['alumno'] = $alumnoModel->find($id);
         /*
         echo '<pre>';
         print_r($data);
         echo '</pre>';
         */
        return view('alumnos/editarAlumnos',$data);
    }
    
    public function editoAlumno($id){
        $alumno = $this->request->getPost();
        unset($alumno['botoncito']);
        /*
        echo '<pre>';
             print_r($alumno);
        echo '</pre>';
        */
        $alumnoModel = new AlumnoModel();
        $alumnoModel->update($id, $alumno);
        return redirect('listado/grupos');
    }
     
} 
    
