<?php


namespace App\Controllers;
use App\Models\GrupoModel; 
use App\Models\AlumnoModel; 


class InsertarController extends BaseController {
    
    public function formGrupos(){
        helper('form');
        $data['title'] = 'Insertar Grupo';
        return view('grupos/insertarGrupos', $data);
    }
    
    public function insertaGrupo(){
        // $codigo = $this->request->getPost('codigo'); 
        // $nombre = $this->request->getPost('nombre');
        $grupo_nuevo = [
            'codigo' => $codigo = $this->request->getPost('codigo'),
            'nombre' => $nombre = $this->request->getPost('nombre')
        ];
    
        //echo '<pre>';
        //print_r($grupo_nuevo);
        //echo '</pre>';
        
        $grupoModel = new GrupoModel();
        $grupoModel->insert($grupo_nuevo);
        return redirect()->to('listado/grupos');
    }
    
    public function formAlumnos(){
        helper('form');
        $data['title'] = 'Insertar Alumno';
        return view('alumnos/insertarAlumnos', $data);
    }
    
    public function insertaAlumno(){

        $grupo_nuevo = $this->request->getPost();
        
        //echo '<pre>';
        //print_r($grupo_nuevo);
        //echo '</pre>';
        
        $alumnoModel = new AlumnoModel();
        $alumnoModel->insert($grupo_nuevo);
        return redirect()->to('listado/grupos');
    }
    
} 
    
