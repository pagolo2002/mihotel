<?php

namespace App\Controllers;
use App\Models\TipohabitacionModel;


class TipohabitacionController extends BaseController
{
    public function hoteles()
    {
        $data['title'] = 'Listado de Hoteles';
        $tipohabitacionModel = new TipohabitacionModel();
        $data['hoteles'] = $tipohabitacionModel->findAll();
        return view('grupos/listadoHoteles', $data);
    }
    
    public function formHoteles(){
        helper('form');
        $data['title'] = 'Insertar Hoteles';
        return view('grupos/insertarHoteles', $data);
    }
    
    public function insertaHotel(){
        // $codigo = $this->request->getPost('codigo'); 
        // $nombre = $this->request->getPost('nombre');
        $grupo_nuevo = [
            'id' => $id = $this->request->getPost('id'),
            'nombre' => $nombre = $this->request->getPost('nombre')
        ];
    
        //echo '<pre>';
        //print_r($grupo_nuevo);
        //echo '</pre>';
        
        $tipohabitacionModel = new TipohabitacionModel();
        $tipohabitacionModel->insert($grupo_nuevo);
        return redirect()->to('listado/hoteles');
        
    }
    
    public function borrarHotel($id){
        $tipohabitacionModel = new TipohabitacionModel();
        $data['hotel'] = $tipohabitacionModel->where('id', $id)->delete();
        return  redirect()->to('listado/hoteles');
    }
    
    public function grupos($id){
        helper('form');
        $tipohabitacionModel = new TipohabitacionModel();
        $data['title'] = 'Modificar Hotel';
        $data['hotel'] = $tipohabitacionModel->find($id);
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';
        return view('grupos/editarHoteles',$data);
    }
    
    public function editoGrupo($id){
        
        helper('form');
        $tipohabitacionModel = new TipohabitacionModel();
        $data['title'] = 'Modificar Hotel';
        $data['hotel'] = $tipohabitacionModel->find($id);
        $hotel = $this->request->getPost();
        unset($hotel['boton']);

        
        $tipohabitacionModel = new TipohabitacionModel();
        $tipohabitacionModel->update($id,$hotel);
        
        if ($tipohabitacionModel->update($id,$hotel)=== false){
            $data['errores'] = $tipohabitacionModel->errors();
            $data['title'] = 'Modificar Hotel';
            return view('grupos/editarHoteles',$data);
        }
        
        return redirect('listado/hoteles');
    }
}
