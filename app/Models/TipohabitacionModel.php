<?php

namespace App\Models;
use CodeIgniter\Model;

class TipohabitacionModel extends Model{ 
    protected $table = 'hoteles';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['id','nombre','descripcion','localidad','direccion','cp','email'];
    
    protected $validationRules = [
        'id'    => 'required',
        'nombre'    => 'required',
    ];
}