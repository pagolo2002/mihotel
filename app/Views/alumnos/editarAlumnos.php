<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <?= form_open('editar/editoAlumno/' . $alumno->id) ?>

    <div class="form-group row">
        <?= form_label('NIA:', 'NIA', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->NIA, ['class' => 'form_control', 'id' => 'NIA']) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Nombre:', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->nombre, ['class' => 'form_control', 'id' => 'nombre']) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('1r Apellido:', 'apellido1', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->apellido1, ['class' => 'form_control', 'id' => 'apellido1']) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('2º Apellido:', 'apellido2', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->apellido2, ['class' => 'form_control', 'id' => 'apellido2']) ?>
        </div>
    </div>


    <div class="form-group row">
        <?= form_label('NIF:', 'nif', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->nif, ['class' => 'form_control', 'id' => 'nif']) ?>
        </div>
    </div>

    <div class="form-group row">
        <?= form_label('Correo Electrónico:', 'email', ['class' => 'col-sm-2 col-form-label']) ?>
        <div class="col-sm-10">
            <?= form_input('codigo', $alumno->email, ['class' => 'form_control', 'id' => 'email']) ?>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-10">
            <?= form_submit('botoncito', 'Enviar', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= form_close() ?>
<?= $this->endSection() ?>