<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('insertar/creaAlumno')?>" method="post">
        <div class="form-group">
            <?= form_label('NIA:', 'NIA', ['class'=>'col-2'])?>
            <?= form_input('NIA',set_value('NIA',''),['class'=>'form_control col-9', 'id'=>'NIA']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('1er Apellido:', 'apellido1', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('apellido1',set_value('apellido1',''),['class'=>'form_control col-9', 'id'=>'apellido1']) ?>
        </div>
        <div class="form-group">
            <?= form_label('2º Apellido:', 'apellido2', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('apellido2',set_value('apellido2',''),['class'=>'form_control col-9', 'id'=>'apellido2']) ?>
        </div>
        <div class="form-group">
            <?= form_label('NIF:', 'nif', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nif',set_value('nif',''),['class'=>'form_control col-9', 'id'=>'nif']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Correo electrónico:', 'email', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('email',set_value('email','algo@correo.com'),['class'=>'form_control col-9', 'id'=>'email']) ?>
        </div>
        <input class="btn btn-primary" type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>
