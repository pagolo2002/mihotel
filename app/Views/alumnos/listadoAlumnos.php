<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('insertar/alumno')?>">Insertar</a>
        </li>
    </ul>


    <table class="table table-striped" id="myTable">
        <thead>
            <th>
                NIA
            </th>
            <th>
                Nombre
            </th>
            <th>
                email
            </th>
            <th>
                Acciones
            </th>
        </thead>
        <tbody>
        <?php foreach ($alumnos as $alumno): ?>
            <tr>
                <td>
                    <?= $alumno->NIA ?>
                </td>
                <td>
                    <?= $alumno->nombre ?> <?= $alumno->apellido1 ?> <?= $alumno->apellido2 ?>
                </td>
                <td>
                    <?= $alumno->email ?>
                </td>
                <td class="text-right">
                    <a href="<?=site_url('editar/alumnos/'.$alumno->id)?>" title="Editar <?= $alumno->nombre.' '.$alumno->apellido1 ?>">
                        <span class="bi bi-pencil-square"></span>
                    </a>
                    
                    <a href="<?=site_url('borrar/alumno/'.$alumno->NIA)?>" onclick="ventanita()">
                        <span class="bi bi-eraser-fill" title="Eliminar el alumno"></span>
                    </a> 
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>

<script>
    function ventanita() {  
        alert("Este grupo se eliminará");  
    }  
</script>

<?= $this->endSection() ?>

