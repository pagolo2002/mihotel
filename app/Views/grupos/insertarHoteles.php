<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('insertar/creaHotel')?>" method="post">

        <div class="form-group">
            <?= form_label('id:', 'codigo', ['class'=>'col-2'])?>
            <?= form_input('codigo',set_value('codigo',''),['class'=>'form_control col-9', 'id'=>'codigo']) ?>
        </div>
        <div class="form-group">
            <?= form_label('nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">

        <input class="btn btn-primary" type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>
