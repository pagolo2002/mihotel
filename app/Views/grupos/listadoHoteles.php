<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<a class="btn btn-primary mb-3" href="http://localhost:8080/mihotel/index.php/insertar/hotel/">Insertar</a> <br>

    <table class="table table-striped" id="myTable">
        <thead>
            <tr>
                <th>
                    id
                </th>
                <th>
                    Nombre
                </th>
                <th>
                    Acciones
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hoteles as $hotel): ?>
                <tr>
                    <td>
                        <?= $hotel->id ?>
                    </td>
                    <td>
                        <?= $hotel->nombre ?>
                    </td>
                    <td class="text-right">
                        <a href="<?=site_url('editar/grupo/'.$hotel->id)?>">
                            <span class="bi bi-pencil-square" title="Editar el grupo"></span>
                        </a>
                        <a href="<?=site_url('borrar/grupo/'.$hotel->id)?>" onclick="ventanita()">
                            <span class="bi bi-eraser-fill" title="Eliminar el grupo"></span>
                        </a>                         
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

<script>
    function ventanita() {  
        alert("Este grupo se eliminará");  
    }  
</script>


<?= $this->endSection() ?>


